import React, {Component} from 'react';
import "./Form.css";

/*
    class App extends Component{
      render(){
        return(
          <div className="wrapper">
            <div className="form-wrapper">
              <h1>Registration Form</h1>
            </div>


          </div>
        )
      }
    }
*/
    export default class Form extends React.Component {
        constructor() {
            super();
        
            this.state = {
              inputHidden: true
            }
          }   


          toggleInput = () => {
            this.setState({
              inputHidden: !this.state.inputHidden
            })
          };
        
          render() {
           
            return (
            <span onSubmit={this.handleSubmit} noValidate>
              <div className="wrapper">
              <div className="form-wrapper">
                <h1>Registration Form</h1>
             </div>
             
          </div>
                <div>
                <input 
                type="text" noValidate
                placeholder="First Name"
                type= {this.state.inputHidden ? 'text' : 'text'} />
                <br/>

                <input 
                type="text" 
                placeholder="Last Name"
                type= {this.state.inputHidden ? 'text' : 'text'} />
                <br/>
                <br/>
                </div>

                {this.state.inputHidden ? <button onClick={this.toggleInput} >Next</button> : null}

                <div>
                <input 
                type="text" 
                placeholder="Birth Year"
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>

                <input 
                type="text" 
                placeholder="User Name:"
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>
                

                <input 
                type="text" 
                placeholder="Password:"
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>
                

                <input 
                type="text" 
                placeholder="Confirm Password:"
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>
                

                <input 
                type="text"
                placeholder="Email:" 
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>
                

                <input 
                type="text" 
                placeholder="Address:"
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>
                

                <input 
                type="text"
                placeholder="ZIP Code:"
                type= {this.state.inputHidden ? 'hidden' : ''} />
                <br/>
                

                <input 
                type="text" 
                placeholder="Country"
                type= {this.state.inputHidden ? 'hidden' : ''} />

                <br/>
                
                <p>Check here to indicate that you have read and agree to terms of <a href="https://www.casino.com/about/terms-and-conditions/">Customer Agreement </a><br/>
                <input name ="iagree" type="checkbox" value="iagree" />I agree</p>
               
                </div>
                {this.state.inputHidden ? null : <button onClick={this.submit} >Register me!</button >}
                
            </span>
            
             )
          }
        }
