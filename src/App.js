import React, { Component } from "react";
import "./App.css";
import logo from "./logo.png";



var dopustenoIme=false;
var dopustenoPrezime=false;
const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const passwordRegex = RegExp(
  /^(?=.*\\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,16}$/
);

const password_confirmRegex = RegExp(
  /^(?=.*\\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,16}$/
);

const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      password: null,
      userName: null,
      checked: false,
      formErrors: {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        userName: "",
        password_confirm: "",
        address: "",
        zipCode: "",
        error: [],
        inputHidden: true
      }
    };
  }

 

  handleCheck = () => {
    this.setState({checked: !this.state.checked});
  }
 
  toggleInput = e => {
    e.preventDefault();
    const { name, value } = e.target;
    if ((this.state.firstName==null) || (this.state.lastName==null)){
        alert("Enter your first or last name!");
    }
    else if (dopustenoIme==false) {
      alert("Enter your first or last name!");
    }
    else if (dopustenoPrezime==false)
    {
    alert("Enter your first or last name!");
    }
    else{
        this.setState({inputHidden: !this.state.inputHidden})
        alert("First step is successful!");
      }
  };

  handleSubmit = e => {
    e.preventDefault();

    if (formValid(this.state) && this.state.checked==true) {
      console.log(`
        --SUBMITTING--
        First Name: ${this.state.firstName}
        Last Name: ${this.state.lastName}
        Email: ${this.state.email}
        Password: ${this.state.password}
        User Name: ${this.state.userName}
        Confirm Password: ${this.state.password_confirm}
        Address: ${this.state.address}
        ZIP Code: ${this.state.zipCode}
        
      `);
      alert("Successful registration!");
    } else {
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
      alert("Registration failed!");
    }
  };

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };
    switch (name) {
      case "firstName":
      if(formErrors.firstName=value.length > 2 ){
        dopustenoIme=true;
      }
      else {
        dopustenoIme=false;
      }
        formErrors.firstName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "lastName":
      if(formErrors.lastName=value.length > 2 ){
            dopustenoPrezime=true;
          }
          else {
            dopustenoPrezime=false;
          } 
        formErrors.lastName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "email":
        formErrors.email = emailRegex.test(value)
          ? ""
          : "invalid email address";
        break;
      case "password":
        // formErrors.password = passwordRegex.test(value) ? "" : "invalid password";
        // break;
        formErrors.password =
        value.length < 3 ? "minimum 3 characaters required" : "";
        break;
        case "userName":
          formErrors.userName = 
            value.length < 2 ? "minimum 2 characaters required" : "";
          break;
        case "password_confirm":
            formErrors.password_confirm=
        (this.state.password != value) ? "invalid confirm pass " : ""
    
            /* formErrors.userName = password_confirmRegex.test(value) ? "" : "invalid password"; */
            break;  
        case "address":
              formErrors.address = 
                value.length < 3 ? "minimum 3 characaters required" : "";
            break; 
        case "zipCode":
              formErrors.zipCode = 
                value.length < 5 ? "5 characaters required" : "";
            break; 

      default:
        break;
    }

    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };


  render() {
    const { formErrors } = this.state;
    return (
      
      <div className="wrapper" id="casino">
        <div className="form-wrapper" style={{height:this.state.inputHidden ? null : "32%"}}>
        
          <div style={{width: "100%", display: "flex", position: "apsolute", top: "0"}}>
            <img src={logo} alt="Logo"/>
          </div>
          <h2>Registration</h2>
          <form onSubmit={this.handleSubmit} noValidate>

            <div className="firstName">
              <label htmlFor="firstName">First Name</label>
              <input
                className={formErrors.firstName.length > 0 ? "error" : null}
                placeholder="First Name"
                type="text"
                name="firstName"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.firstName.length > 0 && (
                <span className="errorMessage">{formErrors.firstName}</span>
              )}
            </div>
            
            <div className="lastName">
              <label htmlFor="lastName">Last Name</label>
              <input
                className={formErrors.lastName.length > 0 ? "error" : null}
                placeholder="Last Name"
                type="text"
                name="lastName"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.lastName.length > 0 && (
                <span className="errorMessage">{formErrors.lastName}</span>
              )}
            </div>
          

            <div className="email">
              <label style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }}  htmlFor="email">Email</label>
              <input
                className={formErrors.email.length > 0 ? "error" : null}
                placeholder="Email"
                type= {this.state.inputHidden ? '' : 'hidden'  } 
                name="email"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.email.length > 0 && (
                <span className="errorMessage">{formErrors.email}</span>
              )}
            </div>

            <div className="userName">
              <label style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }} htmlFor="userName">User Name</label>
              <input
                className={formErrors.firstName.length > 0 ? "error" : null}
                placeholder="User Name"
                type={this.state.inputHidden ? '' : 'hidden'  }
                name="userName"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.userName.length > 0 && (
                <span className="errorMessage">{formErrors.userName}</span>
              )}
            </div>

            <div className="password">
              <label style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }}  htmlFor="password">Password</label>
              <input
                className={formErrors.password.length > 0 ? "error" : null}
                placeholder="Password"
                size="16"
                maxLength="16"
                type={this.state.inputHidden ? '' : 'hidden'  }
                name="password"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.password.length > 0 && (
                <span className="errorMessage">{formErrors.password}</span>
              )}
            </div>
            
            <div className="password">
              <label style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }} htmlFor="password_confirm">Password Confirm</label>
              <input
                className={formErrors.firstName.length > 0 ? "error" : null}
                placeholder="Password Confirm"
                size="16"
                maxLength="16"
                type={this.state.inputHidden ? '' : 'hidden'  }
                name="password_confirm"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.password_confirm.length > 0 && (
                <span className="errorMessage">{formErrors.password_confirm}</span>
              )}
            </div>

            <div className="address">
              <label style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }} htmlFor="address">Address</label>
              <input
                className={formErrors.firstName.length > 0 ? "error" : null}
                placeholder="Address"
                type={this.state.inputHidden ? '' : 'hidden'  }
                name="address"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.address.length > 0 && (
                <span className="errorMessage">{formErrors.address}</span>
              )}
            </div>

            <div className="zipCode">
              <label style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }} htmlFor="zipCode">ZIP Code</label>
              <input
                className={formErrors.firstName.length > 0 ? "error" : null}
                placeholder="ZIP Code"
                maxLength="5"
                type={this.state.inputHidden ? '' : 'hidden'  }
                name="zipCode"
                noValidate
                onChange={this.handleChange}
              />
              {formErrors.zipCode.length > 0 && (
                <span className="errorMessage">{formErrors.zipCode}</span>
              )}
            </div>

            <div style={{visibility: this.state.inputHidden ? 'visible' : 'hidden' }}>
                <label htmlFor="checkbox">
                   <p>Check here to indicate that you have read and agree to terms of <a href="https://www.casino.com/about/terms-and-conditions/">Customer Agreement.</a><br/></p> 
                  <input name="checkbox" type="checkbox" value="check" id="agree"
                  onChange={this.handleCheck} defaultChecked={this.state.checked}/>I agree

                </label>
            </div>

            <div className="create" >
              {this.state.inputHidden ? null: <button onClick={this.toggleInput} style={{marginTop: "-68%"}}>Next</button> }


              {this.state.inputHidden ?  <button onClick={this.submit} >Register me!</button > : null }
            </div>

          </form>
          
        </div>
      </div>
        
      
    );
  }
}



export default App;
